# changes by release

## 0.4
### changes
* *omd* - update to 3.10 - user1/2 resource macro will now use /lib/monitoring-plugins - see [omd commit](https://github.com/ConSol/omd/commit/0350b21e3621cf77ae3abca04f33074c79f28d38)

### added
* *services* - add possibility to specify hostgroup_name and host_name
* *services* - add volatile option

### fixes
* *servicegroups* - add members

## 0.3
### added
* custom notification commands
* *services* - retry_interval, check_period and max_check_attempts possible

### fixes
* fixed contact template

### other
* some various update for OMD 2.90 compatibility
* *omd_config_enable_html_notificiations* replaced with *omd_config_enable_custom_notificiations*

## 0.2
### changes
* Ansible >2.2 is required
* *omd_site_hash* needs to be defined
* *omd_config_hash* has been removed by *omd_config_list*.
* *check_multi*: is now a array of hashes and has been renamed to *omd_naemon_check_multi_list*


### features
* multiple omd sites (alpha)
* add custom ssl certificate to apache
* add cgi.cfg file
* option to use inventory_hostname instead of ansible_hostname for host creation
* *hosts.cfg*: add *use* option

### fixes
* *known_hosts*: use ansible_hostname instead of ansible_fqdn
* *check_multi*: tag is not mandatory anymore
* *commands.cfg*: added missing default notify-by commands

## 0.1
* Initial release

# ansible-role-omd
An Ansible role to manage [OMD-Labs](https://labs.consol.de/omd/) on Debian.

## Requirements
* Ansible >2.2
* OMD 3.10

## Limitations
* Only Debian, tested with Stretch
* Tested with naemon, grafana, influxdb, nagflux, thruk
* No NRPE support (won't do)
* If multiple OMD sites are used _naemon reload_ handler will reload all sites

## Role variables

### OMD

* `omd_site_hash` - Name of the OMD sites
* `omd_admin_password` - Password of omdadmin user
* `omd_release_version` - Release version of omd. stable or testing.
* `omd_config_enable_known_hosts` - Optional management for known_hosts file of OMD site
* `omd_config_list` - Array/List of OMD configuration (site.conf)
* `omd_config_enable_custom_notificiations` - Enable custom notification commands

### Naemon
* `omd_naemon_config_templates_hosts_all` - Default template for all hosts
* `omd_naemon_config_templates_services_all` - Default template for all services
* `omd_naemon_hosts_hash` - Hosts from an Ansible inventory group to manage
* `omd_naemon_hosts_local_hash` - Specify hosts manually, not coming from inventory
* `omd_naemon_hostgroups_hash` - Hostgroups from an Ansible inventory to manage
* `omd_naemon_services_hash` - Services to define
* `omd_naemon_servicegroups_hash` - Servicegroups from an Ansible inventory to manage
* `omd_naemon_commands_hash` - Manage Contact, Host, Service templates
* `omd_naemon_timeperiods_hash` - Manage timeperiods
* `omd_naemon_macros_hash` - Configuration for user macros (resource file)
* `omd_naemon_contacts_hash` - Manage contacts
* `omd_naemon_contactgroups_hash` - Manage contactgroups
* `omd_naemon_templates_hash` - Configure templates for hosts, services and contacts

### check_multi
* `omd_naemon_check_multi_list` - Configuration of a check_multi file

### Thruk
* `omd_thruk_config_cgi_escape_html_tags_enabled` - Switch to [escape html output](https://www.thruk.org/documentation/cgi-cfg.html#escape_html_tags) (mainly for check_multi)

### Apache
* `omd_apache_ssl_cert_custom_enabled` - Switch to enable custom SSL cert
* `omd_apache_ssl_cert_src` - SSL cert source file
* `omd_apache_ssl_key_src` - SSL key source file

## Dependencies
None

## Examples
### OMD

#### Sites
Create OMD sites via `omd_site_hash`.
```yaml
omd_site_hash:
  - name: austria
```

#### Config
```yaml
omd_site_hash:
  - name: austria
    omd_config_list:
      - { name: 'MOD_GEARMAN', value: 'on' }
      - { name: 'GEARMAND_PORT', value: 'srv-mon-01:4730' }
      - { name: 'GRAFANA', value: 'on' }
      - { name: 'INFLUXDB', value: 'on' }
      - { name: 'PNP4NAGIOS', value: 'off' }
      - { name: 'NAGFLUX', value: 'on' }
```

### Templates
Add a new Host/Service template and use it as default
```yaml
- role: omd  
  omd_naemon_templates_hash:
    hosts:
      generic-host-new:
        use: generic-host
        notification_options: w,c,r
        contact_groups: admins
    services:
      generic-service-new:
        use: generic-service
        notification_options: w,c,r
        contact_groups: admins
    # default tmpl unless specified different
    omd_naemon_config_templates_hosts_all: generic-host-new
    omd_naemon_config_templates_services_all: generic-service-new
```

### Macros
```yaml
- role: omd
  omd_naemon_macros_hash:
    USER5: '/usr/local/lib/nagios/plugins'
    USER6: "{{ vault_some_password }}"
```

### Timeperiod
```yaml
- role: omd
  omd_naemon_timeperiods_hash:
    backup:
      alias: backup window
      sunday: "00:00-23:00"
      monday: "00:00-22:00"
```
### Commands
```yaml
- role: omd
  omd_naemon_commands_hash:
    check_gearman:
      line: "$USER1$/check_gearman -H {{ ansible_lo.ipv4.address }}"
    check_dns:
      line: "$USER1$/check_by_ssh -q -H $HOSTADDRESS$ -C \"$USER5$/check_dns $ARG1$\""
    check_nwc_health:
      line: "$USER1$/check_nwc_health --hostname $HOSTADDRESS$ --community {{ vault_snmp_community }} --mode $ARG1$"

```
### Hosts
Add hosts from Ansible inventory group
```yaml
- role: omd
  omd_naemon_hosts_hash:
    # all hosts in the [srv-lin] inventory group
    srv-lin:
      # use another template instead of omd_naemon_config_templates_hosts_all
      use: linux-servers
    cisco_catalyst:
      # if omd_config_enable_known_hosts is enabled don't use it
      known_hosts_disable: true
      # use the hostname from the inventory instead of ansible_hostname fact
      enable_inventory_hostname: true
```

Add hosts manually without Ansible inventory
```yaml
- role: omd
  omd_naemon_hosts_local_hash:
    wlc-01:
      address: wlc.test.at
```
### Groups
```yaml
- role: omd
  # hostgroup
  omd_naemon_hostgroups_hash:
    srv-lin:
      members: "{{ groups['srv-lin'] | map('extract', hostvars, ['ansible_hostname']) | join(', ')}}"
    cisco_catalyst:
      members: "{{ groups['cisco_catalyst'] | map('extract', hostvars, ['ansible_hostname']) | join(', ')}}"
  # servicegroup
  omd_naemon_servicegroups_hash:
    linux:
      members: "{{ groups['linux'] | map('extract', hostvars, ['ansible_hostname']) | join(', *, ') }}, *"
```
### Services
```yaml
- role: omd
  omd_naemon_services_hash:
    check_gearman:
      host_name: "{{ ansible_fqdn }}"
      check_command: check_gearman
    check_dns:
      host_name: dns1.test.at
      check_command: check_dns!www.google.at
    check_cisco_health:
      hostgroup_name: cisco_catalyst
      check_command: "check_nwc_health!hardware-health"
      check_interval: '60'
```
### Contacts
```yaml
- role: omd
  omd_naemon_contacts_hash:
    me:
      email: me@test.at
      use: generic-contact
      alias: me
      service_notification_commands: service-notify-by-email-html
      host_notification_commands: host-notify-by-email-html

  omd_naemon_contactgroups_hash:
    admins:
      members: me
```
### Notification commands
```yaml
- role: omd
  omd_config_enable_custom_notificiations: true
  omd_naemon_notification_commands_hash:
    notify-service-by-zulip: 'command line to send to zulip'
```

### check_multi
Will create two check_multi cmd files *check_perf.cmd*, *check_cisco_perf.cmd* at `OMD_SITE/etc/check_multi`.
```yaml
- role: omd
  omd_naemon_check_multi_list:
    # linux
    - name: check_perf
      check_cpu:
        command: "$USER1$/check_by_ssh -q -H $HOSTADDRESS$ -C \"$USER5$/check_cpu\""
      check_mem:
        command: "$USER1$/check_by_ssh -q -H $HOSTADDRESS$ -C \"$USER5$/check_mem\""
    # cisco
    - name: check_cisco_perf
      check_cpu:
        command: "$USER1$/check_nwc_health --hostname $HOSTADDRESS$ --community {{ vault_snmp_community }} --mode cpu-usage"
      check_mem:
        command: "$USER1$/check_nwc_health --hostname $HOSTADDRESS$ --community {{ vault_snmp_community }} --mode memory-usage"
```

### Apache SSL certifcate
Use a official SSL certificate instead of self-signed.
```yaml
- role: omd
  omd_apache_ssl_cert_custom_enabled: true
  omd_apache_ssl_cert_src: "/path/to/ssl.cert"
  omd_apache_ssl_key_src: "/path/to/ssl.key"
```
